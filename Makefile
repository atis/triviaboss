
QUIZFILE=app/script/quiz

.PHONY: nothing
nothing:
	@echo 'Please specify a target (e.g., "$$ make quiz")'

$(QUIZFILE): $(wildcard moxquizz-0.9.0/quizdata/questions.*.en)
	cat $^ | tr '\267' _ | grep -vE '^#' | grep -E '^Question:|^Answer:|Category:|^[[:space:]]*$$' > $@

.PHONY: quiz
quiz: $(QUIZFILE)

categories: $(QUIZFILE)
	grep -E '^Category:' $< | sort | uniq -c > $@

duplicates: $(QUIZFILE)
	grep -E '^Question:' $< | sort | uniq -cd > $@

.PHONY: clean
clean: $(QUIZFILE) categories
	rm -f $^
