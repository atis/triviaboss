package;

import flash.display.Bitmap;
import flash.display.Sprite;
import openfl.Assets;

class Main extends Sprite {

    public function new() {
        super();

        var bmp_data = Assets.getBitmapData("assets/kaka.png");
        var bmp = new Bitmap(bmp_data);
        addChild(bmp);

        bmp.x = 0;
        bmp.y = 0;
        bmp.scaleX = 0.5;
    }
}
