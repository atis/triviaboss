
package.path = package.path .. ';script/?.lua'

AppConfig = {
    music_volume        = 0.3,  -- Zero volume disables music.
    sound_volume        = 0.3,  -- Zero volume disables sound effects.
    
    window_size         = {width=1024, height=614},
    view_size           = {width=1024, height=614},
    debug               = false,
    fullscreen          = false,
    show_cursor         = false,
    draw_button_lines   = false,
    draw_MOAI_lines     = false,
}
