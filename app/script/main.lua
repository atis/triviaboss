
if AppConfig.debug then
    require("3rdparty/strict")
    inspect = require("3rdparty/inspect")
end
_ = require("3rdparty/underscore")
require("lib/basic_util")
require("lib/texture")
require("lib/moai_util")
require("moai_state")
require("lib/button_behavior")
require("trivia")
require("page")
require("score")
require("sound")
require("ghost")
require("title")
require("game")

io.output(io.stderr)    -- Set stderr as the default output stream.

--
-- Set screen width & height.
--
local screen_size
local desktop_size = {
    width = MOAIEnvironment.horizontalResolution,
    height = MOAIEnvironment.verticalResolution
}
if AppConfig.fullscreen then
    screen_size = desktop_size
else
    screen_size = AppConfig.window_size
end

if not MOAISim.app_started then
    print("APP STARTED FOR THE FIRST TIME")
    MOAISim.app_started = true

    -- Set simulation step duration.
    MOAISim.setStep(1 / 100)

    -- Open a window and set fullscreen if required.
    MOAISim.openWindow("TriviaBoss", screen_size.width, screen_size.height)
    if AppConfig.fullscreen then
        MOAISim.enterFullscreenMode()
    end

    -- Show or hide mouse cursor.
    if AppConfig.show_cursor then
        MOAISim.showCursor()
    else
        MOAISim.hideCursor()
    end
end

-- Set viewport size & scale.
local viewsz = AppConfig.view_size
local viewport = MOAIViewport.new()
viewport:setSize(screen_size.width, screen_size.height)
viewport:setScale(viewsz.width, viewsz.height)
viewport:setOffset(-1, 1)

-- Reset MOAI and various module states (makes reloading safe).
moai_reset()
texture_reset()
touch_reset()
button_behavior.reset()

-- Set frame buffer background (clear) color.
MOAIGfxDevice.getFrameBuffer():setClearColor(30/255, 30/255, 30/255, 1)

-- Main layer.
main_layer = MOAILayer2D.new()
main_layer:setViewport(viewport)
MOAISim.pushRenderPass(main_layer)

-- Load trivia database.
trivia.load_quiz()

-- Arcade font style.
arcade_style = moai.new_textstyle({
    font = moai.new_font("arcade-38", true),
    size = 38
})

-- Gotham (title) font style.
gotham_style = moai.new_textstyle({
    font = moai.new_font("gotham-large", true),
    size = 120
})

misc_tex = tp_import("image/misc", {filter="nearest", premultiply=true})
moai.new_quadprop({
    ptex     = misc_tex,
    frame    = "scanlines",
    layer    = main_layer,
    priority = 0
})

title_screen(function()
    local first_q = trivia.next_question()

    -- Initialize score and page state.
    score_create(main_layer, misc_tex, arcade_style)
    page_create(main_layer, misc_tex, arcade_style)

    play_sound("sound/FIRST_QUESTION.ogg", 0.6)

    -- Animation sequence: show score first, then page.
    score_show(function()
        category_show(first_q.category)
        page_show_first(first_q, function()
            start_game(first_q, main_layer, arcade_style)
        end)
    end)
end)

