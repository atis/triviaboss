
require("lib/basic_util")

local all_questions = {}
local num_q
local dupe_cache = {}

local invalid_chars = "[^ abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"..
                    "!;%%:%?%*%(%)_%+%-=%.,/\"'@#%$&%[%]\\]"
local input_chars = "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]"
local non_input_chars = "[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]"

--
-- Validate category, question, and answer strings. Strip extra whitespace.
--
local function validate(category, question, answer)
    category = strip(category)
    question = strip(question)
    answer   = strip(answer)
    assert(category:len() > 0 and question:len() > 0 and
           answer:len() > 0)

    if question:len() > 350 then
        --print("LONG QUESTION:", question:sub(1, 30))
        return nil
    end

    -- Make sure there are no unrecognized chars in the supplied strings.
    if category:match(invalid_chars) then
        print("Invalid category:", category)
        return nil
    end
    if question:match(invalid_chars) then
        print("Invalid question:", question)
        return nil
    end
    if answer:match(invalid_chars) then
        print("Invalid answer:", answer)
        return nil
    end

    -- Make sure all (answer) spaces are exactly one character long.
    for sp in string.gmatch(answer, "%s+") do
        if sp ~= " " then
            print("Too much whitespace:", answer)
            return nil
        end
    end

    -- If there are any # chars in answer string, then make sure there's exactly
    -- two of them and extract the in-between string.
    local guessable = answer
    local i = answer:find("#")
    if i then
        local j = answer:find("#", i + 1)
        if not j then
            print("Single #:", answer)
            return nil
        end
        if answer:find("#", j + 1) then
            print("More than 2 #:", answer)
            return nil
        end
        guessable = answer:sub(i + 1, j - 1)
        assert(guessable:len() == strip(guessable):len())
    end

    -- Answers should not contain underscores!
    if guessable:find("_") then
        print("Answer contains underscore: ", answer)
        return nil
    end

    -- Be sure that there's at least one guessable character.
    guessable = answer:gsub(non_input_chars, "")
    if guessable:len() == 0 then
        print("Zero-length answer:", answer)
        return nil
    end

    return category, question, answer
end

local longest_category, longest_question, longest_answer

--
-- Save longest category, question and answer for debugging purposes.
--
local function save_longest(category, question, answer)
    if not longest_category or category:len() > longest_category:len() then
        longest_category = category
    end
    if not longest_question or question:len() > longest_question:len() then
        longest_question = question
    end
    if not longest_answer or answer:len() > longest_answer:len() then
        longest_answer = answer
    end
end

--
-- Parse quiz file.
--
local function load_quiz()
    local q_index = 0
    local all = all_questions
    local category, question, answer

    for line in io.lines("script/quiz") do
        local sub_10 = line:sub(1, 10)
        if sub_10 == "Category: " then
            category = line:sub(11)
        elseif sub_10 == "Question: " then
            question = line:sub(11)
        elseif line:sub(1, 8) == "Answer: " then
            answer = line:sub(9)
        else
            assert(strip(line):len() == 0)
            assert((question and answer) or (question == nil and answer == nil))

            if question then
                category, question, answer = validate(category, question, answer)
                if category then
                    save_longest(category, question, answer)

                    all[q_index + 1] = category
                    all[q_index + 2] = question
                    all[q_index + 3] = answer
                    q_index = q_index + 3

                    category = "None"
                    question = nil
                    answer = nil
                end
            end
        end
    end
    num_q = q_index / 3
end

--
-- Replace guessable characters with underscores. Returns the display
-- (guessable) string + full answer that contains no hash chars.
--
local function make_guessable(answer)
    local guessable = answer
    local i = answer:find("#")
    local prefix, suffix = "", ""
    if i then
        local j = answer:find("#", i + 1)
        prefix = answer:sub(1, i-1)
        suffix = answer:sub(j+1)
        guessable = answer:sub(i+1, j-1)
    end
    return prefix..guessable:gsub(input_chars, "_")..suffix,
           prefix..guessable..suffix
end

--
-- Get next trivia question.
--
local function next_question()
    --
    -- Try to avoid duplicates by selecting a random question up to 1K times.
    --
    local category, question, answer
    for i = 1, 1000 do
        local q_index = (good_random(num_q) - 1) * 3
        category = all_questions[q_index + 1]
        question = all_questions[q_index + 2]
        answer   = all_questions[q_index + 3]

        if not dupe_cache[question] then
            dupe_cache[question] = true
            break
        end
    end

    local guessable, clean_answer = make_guessable(answer)
    return {
        category  = category,
        question  = question,
        answer    = clean_answer,
        guessable = guessable
    }
end

--
-- Return longest category, question, answer in standard form.
--
local function extreme_question()
    return {
        category  = longest_category,
        question  = longest_question,
        answer    = longest_answer,
        guessable = make_guessable(longest_answer)
    }
end

trivia = {
    load_quiz        = load_quiz,
    next_question    = next_question,
    extreme_question = extreme_question,

    input_chars     = input_chars,
    non_input_chars = non_input_chars
}
return trivia