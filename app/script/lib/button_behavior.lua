
require("lib/valid")
require("lib/touch")
require("lib/transform")

-- Button module state.
local buttons, layers

--
-- Return true if argument looks like a hotspot.
--
local function is_hotspot(hs)
    return is_table(hs) and
            ((is_number(hs.r) and is_number(hs.x) and is_number(hs.y)) or
             (is_MOAI_prop(hs.prop) and is_number(hs.pad)))
end

--
-- Create button behavior. Config members:
--     hotspot:     circular area: {x=?, y=?, r=?} or
--                  defined by MOAI prop: {prop: MOAIProp2D, pad=?}
--     layer:       MOAILayer used for determining touch location in world
--                  space.
--     depth:       Depth value determines the order in which button are checked
--                  for hits.
--     parent:      MOAITransform that button is attached to.
--     class:       One of predefined button types -- this is actually a
--                  function that is called whenever associated touch is moved.
--     press:       Function executed when button is pressed.
--     release:     Function for when button is released.
--     move:        Function for when associated touch is moved.
local function create(cfg)
    assert(is_table(cfg))
    assert(is_hotspot(cfg.hotspot))
    assert(is_MOAI_transform(cfg.parent) and is_MOAI_layer(cfg.layer))
    assert(is_fn(cfg.class))
    assert(cfg.press == nil or is_fn(cfg.press))
    assert(cfg.release == nil or is_fn(cfg.release))
    assert(cfg.move == nil or is_fn(cfg.move))
    assert(cfg.depth == nil or is_number(cfg.depth))

    -- Add button (sort by depth value).
    cfg.depth = cfg.depth or 0
    table.insert(buttons, cfg)
    table.sort(buttons, function(a, b)
        return a.depth < b.depth
    end)

    -- Save layer reference for later.
    local lay = cfg.layer
    if not layers[lay] then
        layers[lay] = true

        if AppConfig.draw_button_lines then
            -- Create script deck for drawing debugging lines.
            local script_deck = MOAIScriptDeck.new()
            script_deck:setRect(-5, -5, 5, 5)
            script_deck:setDrawCallback(function(index, xOff, yOff, xFlip, yFlip)
                for i = 1, #buttons do
                    local btn = buttons[i]
                    local hs = btn.hotspot
                    local pos = pt_model2world(hs, btn.parent)
                    MOAIDraw.drawCircle(pos.x, pos.y, hs.r, 10)
                end
            end)

            -- Add prop for rendering debugging lines to layer.
            local prop = MOAIProp2D.new()
            prop:setDeck(script_deck)
            lay:insertProp(prop)
        end
    end

    return cfg
end

--
-- Test if world point falls within hotspot area. Note that for now this only
-- works with props and circular hotspots (for which any possible hotspot
-- scaling is ignored).
--
local function point_in_hotspot(p, hotspot, transform)
    assert(is_vector(p) and is_hotspot(hotspot) and
           is_MOAI_transform(transform))

    if type(hotspot) == "table" then
        -- Assume circular hotspot.
        local pos = pt_model2world(hotspot, transform)
        local d = vector_size(vector_sub(p, pos))
        return (d <= hotspot.r)
    else
        -- Assume MOAIProp.
        return hotspot.prop:inside(p.x, p.y, 0, hotspot.pad)
    end
end

--
-- Go through all buttons to see if there exists a button under given point.
--
local function button_for_point(p)
    assert(is_vector(p))

    -- Loop over all buttons starting with the ones with higher depth value.
    for i = #buttons, 1, -1 do
        local btn = buttons[i]
        if point_in_hotspot(p, btn.hotspot, btn.parent) then
            return btn
        end
    end
    return nil
end

--
-- Initialize/reset button module state.
--
local function reset()
    buttons = {}
    layers = {}

    touch_behavior({
        down = function(touch)
            for layer, _ in pairs(layers) do
                local btn = button_for_point(pt_win2world(touch.pos, layer))
                if btn and not btn.touch then
                    btn.touch = touch
                    if btn.press then
                        btn:press()
                    end
                end
            end
        end,
        move = function(touch)
            for i = 1, #buttons do
                local btn = buttons[i]
                if btn.touch == touch then
                    if btn.move then
                        btn:move()
                    end
                    btn:class()
                end
            end
        end,
        up = function(touch)
            for i = 1, #buttons do
                local btn = buttons[i]
                if btn.touch == touch then
                    local tp = pt_win2world(touch.pos, btn.layer)
                    if btn.release then
                        local activate = point_in_hotspot(tp, btn.hotspot, btn.parent)
                        btn:release(activate)
                    end
                    btn.touch = nil
                end
            end
        end,
        cancel = function(touch)
            for i = 1, #buttons do
                local btn = buttons[i]
                if btn.touch == touch then
                    if btn.release then
                        btn:release(false)
                    end
                    btn.touch = nil
                end
            end
        end
    })
end

button_behavior = {
    create = create,
    reset = reset,

    distance_cancel = function(btn)
        local touch = btn.touch
        if vector_size(touch.disp) > 20 then
            if btn.release then
                btn:release(false)
            end
            btn.touch = nil
        end
    end,
    reentry = function(btn)
        local touch = btn.touch
        local tp = pt_win2world(touch.pos, btn.layer)

        if point_in_hotspot(tp, btn.hotspot, btn.parent) then
            if btn.__released then
                btn.__released = false
                if btn.press then
                    btn:press()
                end
            end
        elseif not btn.__released then
            btn.__released = true
            if btn.release then
                btn:release(false)
            end
        end
    end,
    drag = function(btn) end
}
return button_behavior
