
--
-- Basic event stream. Subscribe event listener functions, then push events
-- onto the stream, and the callbacks will be executed with the given arguments.
--
-- Example:
--
-- local stream = event_stream()
-- stream.subscribe(function(arg1, arg2)
--     print("event!", arg1, arg2)
-- end)
--
-- stream.push("hello", "world")
--
function event_stream()
    local listeners = {}
    return {
        subscribe = function(callback)
            assert(is_fn(callback))
            table.insert(listeners, callback)
        end,
        push = function(...)
            for i = 1, #listeners do
                listeners[i](...)
            end
        end
    }
end
