
--
-- Identity function that simply returns its arguments.
--
function identity(...)
    return ...
end

--
-- Function that does nothing
--
function noop()
end

--
-- Mathematical sign function with optional ability to specify what sign is
-- returned for when x==0.
--
function sign(x, zero_sign)
    if x < 0 then
        return -1
    elseif x > 0 then
        return 1
    end
    if not zero_sign or zero_sign == 0 then
        return 0
    end
    return zero_sign < 0 and -1 or 1
end

--
-- From http://lua-users.org/wiki/SimpleRound
--
function round(num, idp)
    local mult = 10^(idp or 0)
    return math.floor(num * mult + 0.5) / mult
end

--
-- Clamp value to given range.
--
function clamp(x, min, max)
    assert(min <= max)
    if x < min then
        return min
    elseif x > max then
        return max
    end
    return x
end

--
-- If value is nil, return opaque black.
-- If value is a number, return opaque grayscale (all color components are set
-- to given value)
-- If alpha is missing, set it to 1.
-- All color values are clamped to [0, 1] range.
--
function to_color(c)
    if not c then
        return {r=0, g=0, b=0, a=1}
    end
    if type(c) == "number" then
        return {r=v, g=v, b=v, a=1}
    end
    if not c.a then
        c.a = 1
    end
    return {
        r = clamp(c.r, 0, 1),
        g = clamp(c.g, 0, 1),
        b = clamp(c.b, 0, 1),
        a = clamp(c.a, 0, 1)
    }
end

--
-- Strip leading and trailing whitespace from string.
--
function strip(s)
    return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

--
-- Generate a better random value (+ auto seed).
-- http://lua-users.org/wiki/MathLibraryTutorial
--
local seed
function good_random(...)
    if not seed then
        math.randomseed(tonumber(tostring(os.time()):reverse():sub(1, 6)))
        seed = true
    end
    return math.random(...)
end
