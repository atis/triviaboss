
require("lib/valid")
require("lib/geometry")

local view_size = AppConfig.view_size

--
-- Convert point from window coordinates into world coordinates using MOAILayer
-- wndToWorld() method.
--
function pt_win2world(p, layer)
    assert(is_MOAI_layer(layer) and is_vector(p))
    local x, y = layer:wndToWorld(p.x, p.y)
    return {x=x, y=y}
end

--
-- Convert point from model coordinates into world coordinates using
-- MOAITransform2D modelToWorld() method.
--
function pt_model2world(p, transform)
    assert(is_vector(p) and is_MOAI_transform(transform))
    local x, y = transform:modelToWorld(p.x, p.y)
    return {x=x, y=y}
end

--
-- Transform point from layout space into world space.
--
function pt_layout2world(p)
    assert(is_vector(p))
    return {
        x = p.x - view_size.width/2,
        y = view_size.height/2 - p.y
    }
end

--
-- Transform rectangular bounding box from layout space into world space.
--
function bbox_layout2world(bb)
    assert(is_bbox(bb))
    return {
        x = bb.x - view_size.width/2,
        y = view_size.height / 2-bb.y,
        width = bb.width,
        height = bb.height
    }
end

--
-- Mirror coords from regular orientation book to reversed (layout space).
--
function pt_mirror_layout(p)
    assert(is_vector(p))
    return {
        x = view_size.width - p.x,
        y = view_size.height - p.y
    }
end

--
-- Mirror corrd from regular orientation book to reversed (world space).
--
function pt_mirror_world(p)
    assert(is_vector(p))
    return {
        x = -p.x,
        y = -p.y
    }
end
