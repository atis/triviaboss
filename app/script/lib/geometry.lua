
--
-- Return true if argument looks like a vector.
--
function is_vector(v)
    return type(v) == "table" and type(v.x) == "number" and
           type(v.y) == "number"
end

--
-- Return true if argument looks like a size vector (width and height
-- properties).
--
function is_size_vector(sz)
    return type(sz) == "table" and sz.width >= 0 and sz.height >= 0
end

--
-- Return true if argument looks like a bounding box.
--
function is_bbox(bb)
    return type(bb) == "table" and type(bb.x) == "number" and
           type(bb.y) == "number" and type(bb.width) == "number" and
           type(bb.height) == "number"
end

--
-- Angle conversion routines.
--
function deg2rad(angle)
    return angle * math.pi / 180
end
function rad2deg(angle)
    return angle * 180 / math.pi
end

function vector_eq(a, b)
    assert(is_vector(a) and is_vector(b))
    return a.x == b.x and a.y == b.y
end

function vector_add(a, b)
    assert(is_vector(a) and is_vector(b))
    return {x=a.x+b.x, y=a.y+b.y}
end

function vector_sub(a, b)
    assert(is_vector(a) and is_vector(b))
    return {x=a.x-b.x, y=a.y-b.y}
end

function vector_dot(a, b)
    assert(is_vector(a) and is_vector(b))
    return a.x*b.x + a.y*b.y
end

function vector_cross(a, b)
    assert(is_vector(a) and is_vector(b))
    return a.x*b.y - a.y*b.x
end

function vector_size(v)
    assert(is_vector(v))
    return math.sqrt(v.x*v.x + v.y*v.y)
end

--
-- If value is nil, return the zero vector.
-- If value is a number, return 2D vector whose both components have that value.
-- If value is already a vector, return it.
--
function to_vector(v)
    if not v then
        return {x=0, y=0}
    end
    if type(v) == "number" then
        return {x=v, y=v}
    end
    return {
        x = v.x or 0,
        y = v.y or 0
    }
end

--
-- Convert argument to lbrt area/bounds form.
--
function to_lbrt(v)
    if not v then
        return {l=0,b=0,r=0,t=0}
    end
    if type(v) == "number" then
        return {l=v,b=v,r=v,t=v}
    end
    if not v.l then v.l = 0 end
    if not v.b then v.b = 0 end
    if not v.r then v.r = 0 end
    if not v.t then v.t = 0 end
    return v
end

--
-- Angle of vector b relative to vector a, in radians. Since angles would not be
-- defined if either a or b are the zero vector, threshold argument is used to
-- determine how close to zero their length has to be for it to be considered
-- a zero vector. Zero angle is returned in these degenerate cases.
--
-- Positive angle direction is counter-clockwise assuming the X axis points to
-- the right and the Y axis points up.
--
-- Note that if the vector directions are exactly opposite each other so they
-- form a 180 degree (PI radian) angle, it is really impossible to say whether
-- it's positive or negative. In this case, the positive direction is picked
-- over the negative and PI is returned.
--
function vector_angle(a, b, threshold)
    assert(is_number(threshold))
    local a_len = vector_size(a)
    if a_len <= threshold then
        return 0
    end
    local b_len = vector_size(b)
    if b_len <= threshold then
        return 0
    end
    local dot = vector_dot(a, b) / a_len / b_len
    if dot > 1 then
        dot = 1
    elseif dot < -1 then
        dot = -1
    end
    local angle = math.acos(dot)
    return vector_cross(a, b) < 0 and -angle or angle
end
