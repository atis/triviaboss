
KEY_ESCAPE = 27
KEY_ENTER  = 13

local default_anchor = {x=0, y=-1}

--
-- Return four values of anchorect rect.
--
local function anchored_rect(size, anchor, rounded)
    assert(anchor == nil or is_vector(anchor))
    assert(is_size_vector(size))
    
    local anchor = anchor or default_anchor
    local w, h = size.width, size.height
    if not rounded then
        return w * anchor.x, h * anchor.y,
               w * (1 + anchor.x), h * (1 + anchor.y)
    end
    return round(w * anchor.x), round(h * anchor.y),
           round(w * (1 + anchor.x)), round(h * (1 + anchor.y))
end

--
-- Call prop:setIndex() with some argument checking.
--
local function set_frame(prop, ptex, frame_name)
    assert(is_MOAI_prop(prop))
    assert(is_PackedTexture(ptex))

    local frame = ptex[frame_name]
    assert(is_PackedTextureFrame(frame))
    prop:setIndex(frame.index)
end

--
-- Create MOAIQuad2D from packed texture.
--
local function new_quad(cfg)
    assert(expected_keys(cfg, {"ptex", "frame", "size", "anchor", "round"}))
    assert(is_PackedTexture(cfg.ptex))
    assert(is_str(cfg.frame) and #cfg.frame > 0)

    -- Create quad.
    local ptex = cfg.ptex
    local quad = MOAIGfxQuad2D.new()
    quad:setTexture(ptex.__texture)

    -- Set UV coords.
    local frame = ptex[cfg.frame]
    assert(is_PackedTextureFrame(frame))
    quad:setUVQuad(unpack(frame.uvQuad))

    -- IMPORTANT! Set "size" property so that we can retrieve it later.
    quad.size = cfg.size or frame.size

    -- Anchored deck.
    quad:setRect(anchored_rect(quad.size, cfg.anchor, cfg.round))
    return quad
end

--
-- Shorthand for creating a new prop.
--
--   layer          Which layer the prop goes onto.
--   parent         (optional) Which transform to set as the resulting prop's
--                  parent.
--   pos            Prop position.
--   deck           Deck to assign to prop.
--
local function new_prop(cfg)
    assert(expected_keys(cfg, {"layer", "parent", "pos", "deck", "priority",
                               "visible", "scale", "index", "blend", "scissor"}))
    assert(is_MOAI_layer(cfg.layer))
    assert(cfg.parent == nil or is_MOAI_transform(cfg.parent))
    assert(cfg.deck == nil or is_MOAI_deck(cfg.deck))
    assert(cfg.priority == nil or is_number(cfg.priority))
    assert(cfg.visible == nil or is_bool(cfg.visible))
    assert(cfg.index == nil or index > 0)
    assert(cfg.blend == nil or (is_str(cfg.blend) and #cfg.blend > 0))
    assert(cfg.scissor == nil or is_MOAI_scissor(cfg.scissor))

    -- Create prop and add it to layer.
    local prop = MOAIProp2D.new()
    cfg.layer:insertProp(prop)

    -- Set blend mode.
    local blend = cfg.blend or "normal"
    if blend == "source" then
        prop:setBlendMode(MOAIProp.GL_SRC_ALPHA, MOAIProp.GL_ONE_MINUS_SRC_ALPHA)
    elseif blend == "normal" then
        prop:setBlendMode(MOAIProp2D.BLEND_NORMAL)
    elseif blend == "add" then
        prop:setBlendMode(MOAIProp2D.BLEND_ADD)
    elseif blend == "multiply" then
        prop:setBlendMode(MOAIProp2D.BLEND_MULTIPLY)
    else
        assert(false, "Invalid blend mode: "..blend)
    end

    -- IMPORTANT! Set "deck" property as a convenience since there's no
    -- getDeck() method for MOAIProp userdata objects.
    prop.deck = cfg.deck

    -- Set parent, deck, priority, visiblity, index, scissor rect.
    prop:setParent(cfg.parent)
    prop:setDeck(prop.deck)
    prop:setPriority(cfg.priority)
    prop:setVisible(cfg.visible)
    prop:setIndex(cfg.index)
    prop:setScissorRect(cfg.scissor)

    -- Set prop position.
    local pos = cfg.pos or {x=0, y=0}
    assert(is_vector(pos))
    prop:setLoc(pos.x, pos.y)

    -- Set scale.
    local scale = cfg.scale and to_vector(cfg.scale) or {x=1, y=1}
    prop:setScl(scale.x, scale.y)

    return prop
end

--
-- Config is a combo of new_prop() and new_deck() cfg values.
--
local function new_quadprop(cfg)
    assert(expected_keys(cfg, {"ptex", "frame", "size", "anchor", "round",
                               "layer", "parent", "priority", "pos", "bbox",
                               "scale", "visible", "index", "blend", "scissor"}))

    -- Create quad & prop.
    local quad = new_quad({
        ptex    = cfg.ptex,
        frame   = cfg.frame,
        size    = cfg.size,
        anchor  = cfg.anchor,
        round   = cfg.round
    })
    local prop = new_prop({
        layer    = cfg.layer,
        parent   = cfg.parent,
        priority = cfg.priority,
        pos      = cfg.pos,
        bbox     = cfg.bbox,
        scale    = cfg.scale,
        visible  = cfg.visible,
        index    = cfg.index,
        blend    = cfg.blend,
        scissor  = cfg.scissor,
        deck     = quad,
    })
    return prop, quad
end

--
-- Create MOAIGfxQuadDeck2D from packed texture.
--
local function new_quaddeck(cfg)
    assert(expected_keys(cfg, {"ptex", "anchor", "frames", "round"}))
    assert(is_PackedTexture(cfg.ptex))
    assert(cfg.anchor == nil or is_vector(cfg.anchor))
    assert(cfg.frames == nil or is_table(cfg.frames))
    assert(cfg.round == nil or is_bool(cfg.round))

    -- Use all frames from packed texture if not given explicitly.
    local ptex = cfg.ptex
    local list = cfg.frames
    if not list then
        list = {}
        for i = 1, ptex.__num_frames do
            table.insert(list, ptex[i].name)
        end
    end

    -- Create the deck.
    local deck = MOAIGfxQuadDeck2D.new()
    deck:setTexture(ptex.__texture)
    deck:reserve(#list)

    -- Construct frames.
    for i = 1, #list do
        local frame_name = list[i]
        local frame = ptex[frame_name]
        deck:setUVQuad(i, unpack(frame.uvQuad))
        local r = frame.geomRect
        deck:setRect(i, anchored_rect(r, cfg.anchor, cfg.round))
    end

    return deck
end

--
-- Create MOAIScissorRect.
--
local function new_scissor(cfg)
    assert(expected_keys(cfg, {"parent", "pos", "size", "scale", "anchor",
                               "round"}))
    assert(cfg.parent == nil or is_MOAI_transform(cfg.parent))
    assert(cfg.pos == nil or is_vector(cfg.pos))

    local sr = MOAIScissorRect.new()
    sr:setRect(anchored_rect(cfg.size, cfg.anchor, cfg.round))
    sr:setParent(cfg.parent)

    -- Set scissor rect position.
    local pos = cfg.pos or {x=0, y=0}
    sr:setLoc(pos.x, pos.y)

    -- Set scale.
    local scale = cfg.scale
    if scale then
        sr:setScl(scale.x, scale.y)
    end

    return sr
end

--
-- Keep running function every number of seconds.
--
local function heartbeat(seconds, func)
    local timer = MOAITimer.new()
    timer:setMode(MOAITimer.LOOP)
    timer:setSpan(seconds)
    timer:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
        func()
    end)
    timer:start()
    return timer
end

--
-- Use MOAITimer to execute function with specified delay.
--
local function delayed(seconds, func)
    assert(seconds >= 0 and is_fn(func))

    -- If zero delay, execute function immediately.
    if seconds == 0 then
        func()
        return nil
    end

    local timer = MOAITimer.new()
    timer:setSpan(seconds)
    timer:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
        func()
    end)
    timer:start()
    return timer
end

--
-- Execute function when timer is done which may be immediately if the timer is
-- no longer busy.
--
local function when_timer_done(timer, func)
    assert(is_MOAI_timer(timer) and is_fn(func))
    if not timer:isBusy() then
        func()
        return
    end
    timer:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
        func()
    end)
end

--
-- Execute function when MOAIAction generates EVENT_STOP event.
--
local function when_action_stops(action, func)
    assert(is_MOAI_action(action) and is_fn(func))
    if not action:isBusy() then
        func()
        return
    end
    action:setListener(MOAIAction.EVENT_STOP, function()
        func()
    end)
end

--
-- Time left for linear animation that is interpolating value [from -> to].
-- The full interpolation takes full_duration seconds.
--
local function time_left(from, to, full_duration, current)
    local diff = to - current
    if math.abs(diff) == 0 then
        return 0
    end
    assert(to ~= from)
    return math.abs(full_duration * (to - current) / (to - from))
end

--
-- Stop ease-driver if argument is not nil and if it's still doing its thing.
--
local function anim_stop(drv)
    assert(drv == nil or is_MOAI_easedriver(drv))
    if drv and drv:isBusy() then
        drv:stop()
    end
end

local function anim_callback_setup(driver, done_callback)
    assert(driver == nil or is_MOAI_easedriver(driver))
    assert(done_callback == nil or is_fn(done_callback))

    -- Set listener for timer-end event and return MOAIEaseDriver.
    if driver and done_callback then
        driver:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
            done_callback()
        end)
    end
    return driver
end

--
-- Utility function to animate scale. Params:
--   transform    MOAI transform to animate
--   to           Number: final scale value.
--   duration     How long it takes for scale to go from "from" value to "to"
--                value. Note that if current transform scale matches the "from"
--                value, then this is the resulting duration. If not, then the
--                duration will be adjusted.
--   driver       (optional) Ease driver to stop before starting new scale
--                action.
--
-- Extra (optional) config options:
--   from         Number value. When calculating scale duration, assume scaling
--                was started from this value.
--   ease         Ease type (see MOAIEaseType).
--   done         Callback to execute when animation is complete.
--
-- Returns MOAIEaseDriver which represents the newly created action.
--
-- Note that if no transition is possible (already at target scale) then
-- anim_scale() will return nil and WILL NOT execute the "done" callback.
--
local function anim_scale(tf, to, duration, cfg)
    assert(expected_keys(cfg, {"driver", "from", "ease", "done"}))
    assert(is_MOAI_transform(tf))
    assert((is_number(to) or is_vector(to)) and duration >= 0)
    assert(cfg.driver == nil or is_MOAI_easedriver(cfg.driver))
    assert(cfg.from == nil or is_number(cfg.from) or is_vector(cfg.from))

    -- Stop existing driver.
    anim_stop(cfg.driver)

    -- Extract values.
    to = to_vector(to)
    local x, y = tf:getScl()
    local from = cfg.from and to_vector(cfg.from) or {x=x, y=y}

    -- Create ease-driver with combined duration.
    local dur = math.max(time_left(from.x, to.x, duration, x),
                         time_left(from.y, to.y, duration, y))
    local drv = tf:seekScl(to.x, to.y, dur, cfg.ease or MOAIEaseType.LINEAR)
    return anim_callback_setup(drv, cfg.done)
end

--
-- Utility function to animate location.
--
local function anim_pos(tf, to, duration, cfg)
    assert(expected_keys(cfg, {"driver", "from", "ease", "done"}))
    assert(is_MOAI_transform(tf))
    assert((is_number(to) or is_vector(to)) and duration >= 0)
    assert(cfg.driver == nil or is_MOAI_easedriver(cfg.driver))
    assert(cfg.from == nil or is_number(cfg.from) or is_vector(cfg.from))

    -- Stop existing driver.
    anim_stop(cfg.driver)

    -- Extract values.
    to = to_vector(to)
    local x, y = tf:getLoc()
    local from = cfg.from and to_vector(cfg.from) or {x=x, y=y}
    
    -- Create ease-driver with combined duration.
    local dur = math.max(time_left(from.x, to.x, duration, x),
                         time_left(from.y, to.y, duration, y))
    local drv = tf:seekLoc(to.x, to.y, dur, cfg.ease or MOAIEaseType.LINEAR)
    return anim_callback_setup(drv, cfg.done)
end

--
-- Utility function to animate color alpha.
--
local function anim_alpha(color, to, duration, cfg)
    assert(expected_keys(cfg, {"driver", "from", "ease", "done"}))
    assert(is_MOAI_color(color))
    assert(is_number(to) and duration >= 0)
    assert(cfg.driver == nil or is_MOAI_easedriver(cfg.driver))
    assert(cfg.from == nil or is_number(cfg.from))

    -- Stop existing driver and get current alpha value.
    anim_stop(cfg.driver)
    local a = color:getAttr(MOAIColor.ATTR_A_COL)
    
    -- Create ease-driver with combined duration.
    local dur = math.max(time_left(cfg.from or a, to, duration, a))
    local drv = color:seekAttr(MOAIColor.ATTR_A_COL, to, dur,
                               cfg.ease or MOAIEaseType.LINEAR)
    return anim_callback_setup(drv, cfg.done)
end

--
-- Create new MOAITransform with given data.
--
local function new_tf(cfg)
    assert(expected_keys(cfg, {"pos", "angle", "scale", "parent"}))

    local tf = MOAITransform2D.new()

    -- Offset.
    local pos = cfg.pos
    if pos then
        pos = to_vector(pos)
        tf:setLoc(pos.x, pos.y)
    end

    -- Rotation.
    local angle = cfg.angle
    if angle then
        assert(is_number(angle))
        tf:setRot(angle)
    end

    -- Scale.
    local scale = cfg.scale
    if scale then
        scale = to_vector(pos)
        tf:setScl(scale.x, scale.y)
    end

    -- Parent.
    tf:setParent(cfg.parent)

    return tf
end

--
-- Create new MOAIColor object.
--
local function new_color(r, g, b, a)
    assert(number_in_range(r, 0, 1) and number_in_range(g, 0, 1) and
           number_in_range(g, 0, 1) and number_in_range(a, 0, 1))
    local c = MOAIColor.new()
    c:setColor(r, g, b, a)
    return c
end

--
-- Make MOAI color inherit its alpha value from another color node.
--
local function inherit_alpha(color, parent)
    assert(is_MOAI_color(color) and is_MOAI_color(parent))
    color:setAttrLink(MOAIColor.ATTR_A_COL, parent, MOAIColor.ATTR_A_COL)
end

--
-- Inherit all color RGBA properties.
--
local function inherit_color(color, parent)
    assert(is_MOAI_color(color) and is_MOAI_color(parent))
    color:setAttrLink(MOAIColor.ATTR_R_COL, parent, MOAIColor.ATTR_R_COL)
    color:setAttrLink(MOAIColor.ATTR_G_COL, parent, MOAIColor.ATTR_G_COL)
    color:setAttrLink(MOAIColor.ATTR_B_COL, parent, MOAIColor.ATTR_B_COL)
    color:setAttrLink(MOAIColor.ATTR_A_COL, parent, MOAIColor.ATTR_A_COL)
end

--
-- Create new font or returned a cached version.
--
local font_cache = {}
local function new_font(name, bmp)
    assert(is_str(name) and #name > 0)

    if font_cache[name] then
        return font_cache[name]
    end
    local font = MOAIFont.new()
    if bmp then
        font:loadFromBMFont("font/"..name..".fnt")

        -- IMPORTANT! Set bmp property so caller code knows what type of font
        -- this is.
        font.bmp = true
    else
        font:load("font/"..name..".ttf")
    end
    font_cache[name] = font

    return font
end

--
-- Create new text style.
--
local function new_textstyle(cfg)
    assert(expected_keys(cfg, {"font", "size", "color", "offset"}))
    assert(is_MOAI_font(cfg.font))
    assert(is_number(cfg.size) and cfg.size > 0)
    assert(cfg.color == nil or is_color(cfg.color))
    assert(cfg.offset == nil or is_vector(offset))

    local c = cfg.color or {r=1, g=1, b=1, a=1}
    local offset = cfg.offset or {x=0, y=0}

    local style = MOAITextStyle.new()
    style:setFont(cfg.font)
    style:setSize(cfg.size)
    style:setColor(c.r, c.g, c.b, c.a or 1)
    --style:setOffset(offset.x, offset.y)

    -- IMPORTANT! Set bmp property so textbox knows this is a 

    return style
end

--
-- Create new text box.
--
local align = {
    left   = MOAITextBox.LEFT_JUSTIFY,
    center = MOAITextBox.CENTER_JUSTIFY,
    right  = MOAITextBox.RIGHT_JUSTIFY,
    top    = MOAITextBox.LEFT_JUSTIFY,
    bottom = MOAITextBox.RIGHT_JUSTIFY
}
local function new_textbox(cfg)
    assert(expected_keys(cfg, {"layer", "parent", "style", "text", "pos",
                               "size", "padding", "h_align", "v_align",
                               "anchor", "round", "scissor", "priority"}))
    assert(is_MOAI_layer(cfg.layer))
    assert(is_MOAI_textstyle(cfg.style))
    assert(cfg.h_align == nil or cfg.h_align == "left" or
           cfg.h_align == "center" or cfg.h_align == "right")
    assert(cfg.v_align == nil or cfg.v_align == "top" or
           cfg.v_align == "center" or cfg.v_align == "bottom")
    assert(is_size_vector(cfg.size))
    assert(cfg.anchor == nil or is_vector(cfg.anchor))
    assert(cfg.parent == nil or is_MOAI_transform(cfg.parent))

    local pos = to_vector(cfg.pos)
    local size = cfg.size
    local h_align = cfg.h_align and align[cfg.h_align] or align["left"]
    local v_align = cfg.v_align and align[cfg.v_align] or align["top"]
    local padding = to_lbrt(cfg.padding)

    -- True width and height without padding.
    local sz = {
        width = size.width - padding.l - padding.r,
        height = size.height - padding.t - padding.b
    }

    local tb = MOAITextBox.new()
    tb:setStyle(cfg.style)
    tb:setString(cfg.text or "")
    tb:setYFlip(true)
    tb:setAlignment(h_align, v_align)

    -- BMP fonts must use a specific shader!
    if cfg.style:getFont().bmp then
        tb:setShader(MOAIShaderMgr.getShader(MOAIShaderMgr.DECK2D_SHADER))
    end

    -- Text box rect.
    tb:setRect(anchored_rect(sz, cfg.anchor, cfg.round))

    -- Prop attributes.
    cfg.layer:insertProp(tb)
    tb:setParent(cfg.parent)
    tb:setScissorRect(cfg.scissor)
    tb:setLoc(pos.x, pos.y)
    tb:setPriority(cfg.priority)
    return tb
end

moai = {
    new_quad       = new_quad,
    new_prop       = new_prop,
    new_quadprop   = new_quadprop,
    new_quaddeck   = new_quaddeck,
    new_scissor    = new_scissor,
    new_tf         = new_tf,
    new_color      = new_color,
    new_font       = new_font,
    new_textstyle  = new_textstyle,
    new_textbox    = new_textbox,

    delayed           = delayed,
    heartbeat         = heartbeat,
    when_timer_done   = when_timer_done,
    when_action_stops = when_action_stops,

    set_frame         = set_frame,
    anim_scale        = anim_scale,
    anim_pos          = anim_pos,
    anim_alpha        = anim_alpha,
    anim_stop         = anim_stop,
    inherit_alpha     = inherit_alpha,
    inherit_color     = inherit_color
}
return moai
