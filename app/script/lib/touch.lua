
require("lib/geometry")

-- Touch type shorthand.
local TOUCH_DOWN = MOAITouchSensor.TOUCH_DOWN
local TOUCH_MOVE = MOAITouchSensor.TOUCH_MOVE
local TOUCH_UP = MOAITouchSensor.TOUCH_UP
local TOUCH_CANCEL = MOAITouchSensor.TOUCH_CANCEL

-- Module state variables.
local touches, handlers, is_callback_set, warning_printed

--
-- Initialize/reset touch module state.
--
function touch_reset()
    touches = {}
    handlers = {}
    is_callback_set = false
    warning_printed = false
end

--
-- Return true if argument looks like a touch.
--
function is_touch(t)
    return is_table(t) and is_number(t.id) and is_vector(t.pos)
end

--
-- Execute registered touch handlers.
--
local function run_handlers(t, event)
    assert(is_touch(t) and is_str(event))

    for i = 1, #handlers do
        local fn = handlers[i][event]
        if fn then
            fn(t)
        end
    end
end

--
-- Create new touch.
--
local function new_touch(id, x, y)
    assert(is_number(id) and is_number(x) and is_number(y))
    local pos = {x=x, y=y}
    local now = MOAISim.getDeviceTime()
    return {
        id              = id,
        pos             = pos,
        first_pos       = pos,
        prev_pos        = pos,
        first_timestamp = now,
        timestamp       = now,
        prev_timestamp  = now,
        disp            = {x=0, y=0},
        delta           = {x=0, y=0}
    }
end

--
-- Return true if touch position has changed.
--
local function touch_moved(t, x, y)
    assert(is_touch(t) and is_number(x) and is_number(y))
    local prev_pos = t.prev_pos
    return (x ~= prev_pos.x) or (y ~= prev_pos.y)
end

--
-- Update touch position.
--
local function update_touch(t, x, y)
    assert(is_touch(t) and is_number(x) and is_number(y))

    local pos = {x=x, y=y}
    t.prev_pos = t.pos
    t.pos = pos
    t.prev_timestamp = t.timestamp
    t.timestamp = MOAISim.getDeviceTime()
    t.disp = vector_sub(pos, t.first_pos)
    t.delta = vector_sub(pos, t.prev_pos)
end

--
-- Set MOAI touch handler callback.
--
local function set_touch_callback()
    MOAIInputMgr.device.touch:setCallback(function(ev_type, id, x, y, tap_count)
        -- Run touch button handlers.
        if ev_type == TOUCH_DOWN then
            local t = new_touch(id, x, y)
            touches[id] = t
            run_handlers(t, "down")
        elseif ev_type == TOUCH_MOVE then
            local t = touches[id]
            if t and touch_moved(t, x, y) then
                update_touch(t, x, y)
                run_handlers(t, "move")
            end
        elseif ev_type == TOUCH_UP or ev_type == TOUCH_CANCEL then
            local t = touches[id]
            if t then
                update_touch(t, x, y)
                run_handlers(t, ev_type == TOUCH_UP and "up" or "cancel")
                touches[id] = nil   -- Remove touch.
            end
        end
    end)
end

--
-- Emulate touch using mouse.
--
local function set_mouse_callback()
    local id = 666          -- Special ID for mouse-emulated touch.
    local mouse_x = 0
    local mouse_y = 0
    local LMB_down = false
    MOAIInputMgr.device.pointer:setCallback(function(x, y)
        assert(is_number(x) and is_number(y))

        -- Update mouse position.
        mouse_x = x
        mouse_y = y

        if LMB_down then
            local t = touches[id]
            if t and touch_moved(t, x, y) then
                update_touch(t, x, y)
                run_handlers(t, "move")
            end
        end
    end)
        
    MOAIInputMgr.device.mouseLeft:setCallback(function(down)
        assert(is_bool(down))

        if down then
            LMB_down = true

            local t = new_touch(id, mouse_x, mouse_y)
            touches[id] = t
            run_handlers(t, "down")
        else
            LMB_down = false

            local t = touches[id]
            if t then
                update_touch(t, mouse_x, mouse_y)
                run_handlers(t, "up")
                touches[id] = nil   -- Remove touch.
            end
        end
    end)
end

--
-- Register touch handler functions.
--
-- Configuration table members (optional):
--   "down", "move", "up", "cancel"
-- They are all functions executed when respective touch event is received.
--
function touch_behavior(cfg)
    assert(expected_keys(cfg, {"down", "move", "up", "cancel"}))
    assert(cfg.down == nil or is_fn(cfg.down))
    assert(cfg.move == nil or is_fn(cfg.move))
    assert(cfg.up == nil or is_fn(cfg.up))
    assert(cfg.cancel == nil or is_fn(cfg.cancel))

    -- Set MOAI touch handler callback if possible.
    if not is_callback_set then
        if not MOAIInputMgr.device.touch then
            if not warning_printed then
                print("WARNING: Touch device not available.")
                warning_printed = true
            end
            return
        end

        -- Set touch-handling callback.
        set_touch_callback()
        is_callback_set = true

        -- Also emulate touch input using mouse for development purposes.
        if MOAIInputMgr.device.pointer then
            set_mouse_callback()
        end
    end

    table.insert(handlers, cfg)
end
