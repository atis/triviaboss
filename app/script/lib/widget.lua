
--
-- A widget is an object that can both execute commands and be observerd. The
-- execution of commands may take time (like visual transitions).
--
-- Configuration table has the following keys:
--   is_busy      function(cfg)
--                Function with is only argument the original configuration
--                table. It must return true when widget is busy executing a
--                command.
--   exec         function(cfg, cmd, complete_callback)
--                "cmd" argument is the command that must be executed. If the
--                command is accepted and executed, then after it's done
--                executing, the "complete_callback" function must be called.
--   cancel       (optional) Function that terminates any current activity. It
--                must do this immediately. If not present, it is assumed that
--                commands must complete before new ones are executed.
--
-- The returned function is then the one that user can feed commands into.
--
function widget(cfg)
    assert(is_table(cfg))
    assert(is_fn(cfg.is_busy))
    assert(is_fn(cfg.exec))
    assert(cfg.cancel == nil or is_fn(cfg.cancel))

    local observers = {}
    local function notify_observers(cmd, progress)
        for i = 1, #observers do
            observers[i](cfg, cmd, progress)
        end
    end

    -- User may destroy widget in response to some command.
    cfg.destroy = function()
        cfg.is_busy = nil
        cfg.exec = nil
        cfg.cancel = nil
    end

    local next_command
    local function exec(cmd)
        -- Do nothing if widget has been destroyed (exec function undefined).
        if not cfg.exec then
            return
        end

        -- See if widget is already busy executing a command.
        next_command = cmd
        if cfg:is_busy() then
            if not cfg.cancel then
                return
            end
            -- Cancel ongoing command execution.
            cfg:cancel()
            notify_observers(cmd, "cancel")
        end

        -- Execute given command.
        notify_observers(cmd, "start")
        cfg:exec(cmd, function()
            notify_observers(cmd, "end")
            exec(next_command)
        end)
    end
    local function observe(event_callback)
        table.insert(observers, event_callback)
    end

    return exec, observe
end

--
-- "state_cfg" may either be a table that maps states to their functions or it
-- may be a single function that accepts any state.
--
-- All user state functions MUST call the "complete" callback when they are done
-- with their transitions/animations. User functions are responsible for setting
-- widget.state variable.
--
function stateful_widget(state_cfg, initial)
    assert(is_table(state_cfg) or is_fn(state_cfg))

    -- Function that alters state.
    local busy = false
    local function set_state(w, new_state, complete)
        busy = true
        complete = complete or noop
        if type(state_cfg) == "table" then
            state_cfg[new_state](w, new_state, function()
                busy = false
                complete()
            end)
        else
            state_cfg(w, new_state, function()
                busy = false
                complete()
            end)
        end
    end

    -- Widget configuration.
    local widget_cfg = {
        is_busy = function()
            return busy
        end,
        exec = function(w, new_state, complete)
            -- Do nothing if already in requested state.
            if w.state == new_state then
                return
            end

            -- Change state.
            set_state(w, new_state, complete)
        end
    }
    local exec, observe = widget(widget_cfg)

    -- Set initial state.
    if initial then
        set_state(widget_cfg, initial)
    end
    return exec, observe
end

--
-- Interruptible widget.
--
function interruptible_widget(state_cfg, stop, initial)
    assert(is_table(state_cfg) or is_fn(state_cfg))
    assert(stop == nil or is_fn(stop))

    -- Function that alters state.
    local function set_state(w, new_state, complete)
        complete = complete or noop
        if type(state_cfg) == "table" then
            state_cfg[new_state](w, new_state, complete)
        else
            state_cfg(w, new_state, complete)
        end
    end

    -- Widget configuration.
    local widget_cfg = {
        is_busy = function()
            return false
        end,
        exec = function(w, new_state, complete)
            -- Change state.
            set_state(w, new_state, complete)
        end,
        cancel = function()
            if stop then
                stop()
            end
        end
    }
    local exec, observe = widget(widget_cfg)

    -- Set initial state.
    if initial then
        set_state(widget_cfg, initial)
    end
    return exec, observe
end
