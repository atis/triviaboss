--
-- Validation of basic types. Modules that define their own composite "types"
-- should also define their own validation routines.
--

function is_userdata(u)
    return type(u) == "userdata"
end

function is_fn(f)
    return type(f) == "function"
end

function is_table(t)
    return type(t) == "table"
end

function is_bool(b)
    return type(b) == "boolean"
end

function is_number(n)
    return type(n) == "number"
end

function is_str(s)
    return type(s) == "string"
end

function is_goodstr(s)
    return type(s) == "string" and #s > 0
end

function number_in_range(n, min, max)
    return n >= min and n <= max
end

function is_color(c)
    return is_table(c) and number_in_range(c.r, 0, 1) and
           number_in_range(c.g, 0, 1) and number_in_range(c.b, 0, 1) and
           (c.a == nil or number_in_range(c.a, 0, 1))
end

function is_MOAI_node(n)
    return is_userdata(n) and is_fn(n.setAttr)
end
function is_MOAI_color(c)
    return is_userdata(c) and is_fn(c.setColor)
end
function is_MOAI_transform(t)
    return is_userdata(t) and is_fn(t.setLoc)
end
function is_MOAI_prop(p)
    return is_userdata(p) and is_fn(p.setDeck)
end
function is_MOAI_deck(d)
    return is_userdata(d) and is_fn(d.setTexture)
end
function is_MOAI_layer(l)
    return is_userdata(l) and is_fn(l.insertProp)
end
function is_MOAI_scissor(sr)
    return is_userdata(sr) and is_fn(sr.setScissorRect)
end
function is_MOAI_action(a)
    return is_userdata(a) and is_fn(a.isBusy)
end
function is_MOAI_easedriver(ed)
    return is_userdata(ed) and is_fn(ed.reserveLinks)
end
function is_MOAI_timer(t)
    return is_userdata(t) and is_fn(t.setSpan)
end
function is_MOAI_texture(t)
    return is_userdata(t) and is_fn(t.load)
end
function is_MOAI_font(f)
    return is_userdata(f) and is_fn(f.setCache)
end
function is_MOAI_textstyle(ts)
    return is_userdata(ts) and is_fn(ts.getFont)
end

function is_PackedTexture(t)
    return is_table(t) and is_MOAI_texture(t.__texture) and
           t.__num_frames > 0
end
function is_PackedTextureFrame(f)
    return is_table(f) and f.index > 0 and is_table(f.uvQuad) and
           is_table(f.geomRect) and is_table(f.size)
end

--
-- Make sure table "t" only has expected keys (listed in 2nd argument array).
--
function expected_keys(t, keys)
    assert(is_table(t) and is_table(keys))
    local keymap = {}
    for i = 1, #keys do
        keymap[keys[i]] = true
    end
    for k, _ in pairs(t) do
        if not keymap[k] then
            print("Unexpected key:", k)
            return false
        end
    end
    return true
end
