
-- Split a string.
-- Source: http://lua-users.org/wiki/SplitJoin
-- Compatibility: Lua-5.1
local function split(str, pat)
    assert(type(str) == "string" and type(pat) == "string")

    local t = {}  -- NOTE: use {n = 0} in Lua-5.0
    local fpat = "(.-)" .. pat
    local last_end = 1
    local s, e, cap = str:find(fpat, 1)
    while s do
        if s ~= 1 or cap ~= "" then
            table.insert(t,cap)
        end
        last_end = e+1
        s, e, cap = str:find(fpat, last_end)
    end
    if last_end <= #str then
        cap = str:sub(last_end)
        table.insert(t, cap)
    end
    return t
end

-- Split a file path string into components.
-- Source: http://lua-users.org/wiki/SplitJoin
local function split_path(str)
    return split(str,'[\\/]+')
end

local texture_cache     -- texture cache: filename -> MOAITexture

local filter_map = {
    nearest = MOAITexture.GL_NEAREST,
    linear  = MOAITexture.GL_LINEAR
}

local function tex_cache_name(filename, cfg)
    assert(type(filename) == "string" and #filename > 0)
    assert(is_table(cfg))
    assert(cfg.filter == "nearest" or cfg.filter == "linear")

    local truecolor = cfg.truecolor and true or false
    local premultiply = cfg.premultiply and true or false
    return filename..";filter="..cfg.filter
                   ..";truecolor="..tostring(truecolor)
                   ..";premultiply="..tostring(premultiply)
end

--
-- Load texture or return it from cache if already loaded.
--
function texture_load(filename, cfg)
    assert(type(filename) == "string" and #filename > 0 and is_table(cfg))

    -- Check cache.
    local cache_name = tex_cache_name(filename, cfg)
    local tex = texture_cache[cache_name]
    if tex then
        return tex
    end

    -- XXX This should be using bitwise OR.
    local transform = 0
    if cfg.truecolor then
        transform = MOAIImage.TRUECOLOR
    end
    if cfg.premultiply then
        transform = MOAIImage.PREMULTIPLY_ALPHA
    end

    -- New texture.
    tex = MOAITexture.new()
    tex:setFilter(filter_map[cfg.filter])
    tex:load(filename, transform, cache_name)
    texture_cache[cache_name] = tex
    print("Loaded texture:", cache_name)
    return tex
end

--
-- TexturePacker import. Originally from here:
--   http://www.moaisnippets.info/fixed-texturepacker-importer
--
function tp_import(filename, cfg)
    assert(type(filename) == "string" and #filename > 0 and is_table(cfg))

    -- Load atlas (frame data).
    local path = split_path(filename)
    local folder = filename:sub(1, filename:len()-path[#path]:len())
    local ok, atlas = pcall(dofile, filename..".lua")
    if not ok then
        print("WARNING: Failure to load texture \""..filename.."\": "..atlas)
        return nil
    end

    -- Load texture and create import result.
    local import_result = {
        __texture    = texture_load(folder..atlas.texture, cfg),
        __num_frames = #atlas.frames
    }

    -- Process frames.
    for i, frame in ipairs(atlas.frames) do
        local uv = frame.uvRect
        if not frame.textureRotated then
            -- From Moai docs: "Vertex order is clockwise from upper left (xMin, yMax)"
            frame.uvQuad = {uv.u0,uv.v0, uv.u1,uv.v0, uv.u1,uv.v1, uv.u0, uv.v1}
        else
            -- Sprite data is rotated 90 degrees CW on the texture
            -- u0v0 is still the upper-left
            frame.uvQuad = {uv.u1,uv.v0, uv.u1,uv.v1, uv.u0,uv.v1, uv.u0,uv.v0}
        end

        -- Store frame data both as array and mapping (name -> info).
        local path = split_path(frame.name)
        local name = path[#path]:match("(.-)%.png$")
        local frame_data = {
            index    = i,
            name     = name,
            uvQuad   = frame.uvQuad,
            size     = frame.spriteSourceSize,
            geomRect = frame.spriteColorRect,
        }
        import_result[name] = frame_data
        import_result[i] = frame_data
    end        

    -- Return result.
    return import_result
end

--
-- Reset texture cache.
--
function texture_reset()
    texture_cache = {}
end
