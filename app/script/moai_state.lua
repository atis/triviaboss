
require("lib/valid")

local show_lines = nil

function toggle_debug_lines(show)
    assert(show == nil or is_bool(show))

    if show ~= nil then
        show_lines = show
    else
        show_lines = not show_lines
    end

    if show_lines then
        MOAIDebugLines.setStyle(MOAIDebugLines.PARTITION_CELLS, 2, 0, 0, 1, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.PARTITION_PADDED_CELLS, 1, 0, 1, 0, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.PROP_MODEL_BOUNDS, 2, 1, 1, 0, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.PROP_WORLD_BOUNDS, 2, 1, 0, 0, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.TEXT_BOX, 2, 1, 0, 1, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.TEXT_BOX_BASELINES, 2, 1, 1, 0, 1)
        MOAIDebugLines.setStyle(MOAIDebugLines.TEXT_BOX_LAYOUT, 2, 1, 1, 0, 1)
    else
        MOAIDebugLines.showStyle(MOAIDebugLines.PARTITION_CELLS, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.PARTITION_PADDED_CELLS, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.PROP_MODEL_BOUNDS, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.PROP_WORLD_BOUNDS, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.TEXT_BOX, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.TEXT_BOX_BASELINES, false)
        MOAIDebugLines.showStyle(MOAIDebugLines.TEXT_BOX_LAYOUT, false)
    end
end

-- Reset MOAI render, action tree and input state.
function moai_reset()
    MOAISim.clearRenderStack()
    MOAIActionMgr.setRoot(nil)
    MOAIInputMgr.device.pointer:setCallback(nil)
    if MOAIInputMgr.device.touch then
        MOAIInputMgr.device.touch:setCallback(nil)
    end

    toggle_debug_lines(AppConfig.draw_MOAI_lines)
end
