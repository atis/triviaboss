
function title_screen(next_screen)
    if AppConfig.debug then
        return next_screen()
    end

    music_play("music/title.ogg", 1, false, 0, 0)

    -- Title animation.
    local view_size = AppConfig.view_size
    local x_middle = view_size.width / 2
    local title_parent_tf = moai.new_tf({
        pos = {x=0, y=0}
    })
    local title_tb = moai.new_textbox({
        layer   = main_layer,
        parent  = title_parent_tf,
        style   = gotham_style,
        text    = "Trivia Boss",
        h_align = "center",
        v_align = "center",
        pos     = {x=x_middle, y=-200},
        size    = {width=900, height=200},
        anchor  = {x=-0.5, y=-0.5}
    })
    title_tb:setReveal(0)
    title_tb:setSpeed(8)
    moai.delayed(1.2, function()
        title_tb:spool()
    end)

    local info_txt = "Guess answers to trivia questions Hangman-style!\n"..
                     "( use letter and number keys on your keyboard )"

    moai.delayed(4.3, function()
        local base_tf = moai.new_tf({
            pos = {x=0, y=0}
        })

        local info_tb = moai.new_textbox({
            layer   = main_layer,
            parent  = base_tf,
            style   = arcade_style,
            text    = info_txt,
            h_align = "center",
            v_align = "center",
            pos     = {x=x_middle, y=-350},
            size    = {width=900, height=200},
            anchor  = {x=-0.5, y=-0.5}
        })
        info_tb:setLineSpacing(10)

        local press_tb = moai.new_textbox({
            layer   = main_layer,
            parent  = base_tf,
            style   = arcade_style,
            text    = "Hit ENTER to begin",
            h_align = "center",
            v_align = "center",
            pos     = {x=x_middle, y=-480},
            size    = {width=900, height=200},
            anchor  = {x=-0.5, y=-0.5}
        })
        local color = moai.new_color(1, 1, 1, 0.2)
        moai.inherit_alpha(press_tb, color)
        local drv = color:seekColor(1, 1, 1, 1, 0.9, MOAIEaseType.LINEAR)
        drv:setMode(MOAITimer.PING_PONG)

        MOAIInputMgr.device.keyboard:setCallback(function(key, down)    
            if not down then
                return
            end
            if key == KEY_ESCAPE then
                os.exit()
            end
            if key == KEY_ENTER then
                -- Disable input.
                MOAIInputMgr.device.keyboard:setCallback(nil)

                -- Textboxes opening up.
                moai.anim_pos(title_parent_tf, {x=0, y=300}, 1, {
                    ease = MOAIEaseType.EASE_OUT,
                    done = function()
                        main_layer:removeProp(title_tb)
                    end
                })
                moai.anim_pos(base_tf, {x=0, y=-700}, 1, {
                    ease = MOAIEaseType.EASE_OUT,
                    done = function()
                        main_layer:removeProp(info_tb)
                        main_layer:removeProp(press_tb)
                    end
                })

                -- Halt music and go to next screen.
                music_stop(2)
                moai.delayed(1, next_screen)
            end
        end)
    end)
end

function ending_screen()
    music_play("music/joseph-wagner.ogg", 1, false, 1, 0)

    -- Title animation.
    local view_size = AppConfig.view_size
    local x_middle = view_size.width / 2
    local title_parent_tf = moai.new_tf({
        pos = {x=0, y=0}
    })
    local title_tb = moai.new_textbox({
        layer   = main_layer,
        parent  = title_parent_tf,
        style   = gotham_style,
        text    = "Congratulations!",
        h_align = "center",
        v_align = "center",
        pos     = {x=x_middle, y=-100},
        size    = {width=1024, height=200},
        anchor  = {x=-0.5, y=-0.5}
    })
    title_tb:setReveal(0)
    title_tb:setSpeed(8)
    moai.delayed(1, function()
        title_tb:spool()
    end)

    local info_txt = "You are indeed an incredibly intelligent and "..
                     "knowledgeable person. A true Trivia Boss.\n\n"..
                     "Now go do something useful with that wonderful brain of yours!\n\n\n\n"..
                     "THANK YOU FOR PLAYING!"

    moai.delayed(4.3, function()
        local info_tb = moai.new_textbox({
            layer   = main_layer,
            parent  = base_tf,
            style   = arcade_style,
            text    = info_txt,
            h_align = "center",
            v_align = "center",
            pos     = {x=x_middle, y=-340},
            size    = {width=900, height=500},
            anchor  = {x=-0.5, y=-0.5}
        })
        info_tb:setSpeed(8)
        info_tb:spool()

        MOAIInputMgr.device.keyboard:setCallback(function(key, down)    
            if not down then
                return
            end
            if key == KEY_ESCAPE then
                os.exit()
            end
        end)
    end)
end
