
local duration = 0.2

function ghost(char, correct, layer, text_style)
    assert(is_goodstr(char))
    assert(is_bool(correct))
    assert(is_MOAI_layer(layer))
    assert(is_MOAI_textstyle(text_style))

    local view_sz = AppConfig.view_size

    -- Incorrect guesses are displayed red(der).
    local c = correct and {r=1,g=1,b=1,a=1} or {r=1,g=0,b=0,a=1}

    local parent = moai.new_tf({
        pos = {x=view_sz.width / 2, y=-view_sz.height / 2},
    })
    local parent_color = moai.new_color(c.r, c.g, c.b, c.a)

    local tb = moai.new_textbox({
        layer   = layer,
        parent  = parent,
        style   = text_style,
        size    = {width=50, height=50},
        text    = char,
        h_align = "center",
        v_align = "center",
        anchor  = {x=-0.5, y=-0.5}
    })
    moai.inherit_color(tb, parent_color)

    parent_color:seekColor(c.r, c.g, c.b, 0, duration, MOAIEaseType.LINEAR)
    moai.anim_scale(parent, 10, duration, {
        ease = MOAIEaseType.LINEAR,
        done = function()
            layer:removeProp(tb)
        end
    })
end

local catmap = {}
local cat_tex, cat_prop, cat_deck
local offscreen_pos = {x=1030, y=-300}
local onscreen_pos = {x=700, y=-300}

--
-- Hide category image by sliding it out of view.
--
local function category_hide(callback)
    local drv = moai.anim_pos(cat_prop, offscreen_pos, 0.6, {
        ease = MOAIEaseType.EASE_IN,
        done = callback
    })
    if not drv then
        if callback then
            callback()
        end
    end
end

--
-- Show category image in the lower right screen corner.
--
function category_show(category)
    if not cat_prop then
        for line in io.lines("script/catmap") do
            local i = line:find(" ")
            if i then
                catmap[strip(line:sub(i+1))] = line:sub(1, i-1)
            end
        end

        cat_tex = tp_import("image/category", {filter="nearest", premultiply=true})
        cat_deck = moai.new_quaddeck({
            ptex = cat_tex
        })

        cat_prop = moai.new_prop({
            layer    = main_layer,
            deck     = cat_deck,
            priority = -1,
            pos      = offscreen_pos
        })
        cat_prop:setColor(0.3, 0.3, 0.3, 1)
    end

    local frame = cat_tex[catmap[category]]
    category_hide(function()
        if frame then
            cat_prop:setDeck(cat_deck)
            cat_prop:setIndex(frame.index)
        else
            cat_prop:setDeck(nil)
        end
        moai.anim_pos(cat_prop, onscreen_pos, 0.6, {
            ease = MOAIEaseType.EASE_IN
        })
    end)
end
