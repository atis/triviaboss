
local slots_per_row = 5
local max_misses = 10
local slot_tf
local slot_backgrounds = {}
local miss_props = {}

local function add_miss_prop(char, misses)
    assert(is_goodstr(char))
    assert(misses >= 0)

    local pos = {x= 48 * (misses % slots_per_row),
                 y=-48 * math.floor(misses / slots_per_row) + 2}
    table.insert(miss_props, moai.new_textbox({
        layer    = main_layer,
        parent   = slot_tf,
        style    = arcade_style,
        text     = char,
        pos      = pos,
        size     = {width=40, height=40},
        anchor   = {x=-0.5, y=-0.5},
        h_align  = "center",
        v_align  = "center",
        priority = 2
    }))
end

local function clear_miss_props()
    for i = 1, #miss_props do
        main_layer:removeProp(miss_props[i])
    end
end

local function create_slot_backgrounds()
    slot_tf = moai.new_tf({
        pos = {x=762, y=-180}
    })

    local floor = math.floor
    for i = 0, max_misses - 1 do
        if not slot_backgrounds[i] then
            moai.delayed(0.5 + i / 4, function()
                slot_backgrounds[i] = moai.new_quadprop({
                    ptex     = misc_tex,
                    frame    = "miss_bg",
                    layer    = main_layer,
                    parent   = slot_tf,
                    pos      = {x= 48 * (i % slots_per_row),
                                y=-48 * floor(i / slots_per_row)},
                    anchor   = {x=-0.5, y=-0.5},
                    round    = true,
                    priority = 1
                })
            end)
        end
    end
end

function remove_extra_slots()
    for i = max_misses, #slot_backgrounds do
        main_layer:removeProp(slot_backgrounds[i])
    end    
end

function guess(q, user_char)
    local g = q.guessable
    local ans = q.answer
    local new_guessable = ""
    local num_correct = 0
    for i = 1, #g do
        local g_char = g:sub(i, i)
        if g_char == "_" then
            local a_char = ans:sub(i, i)
            if a_char:upper() == user_char then
                new_guessable = new_guessable..a_char
                num_correct = num_correct + 1
            else
                new_guessable = new_guessable.."_"
            end
        else
            new_guessable = new_guessable..g_char
        end
    end

    q.guessable = new_guessable
    return num_correct
end

function start_game(q, layer, text_style)
    assert(is_table(q))
    assert(is_MOAI_layer(layer))
    assert(is_MOAI_textstyle(text_style))

    create_slot_backgrounds()
    music_play("music/loop.ogg", 0.6, true, 0, 1)

    -- Scoring.
    local score = 0
    local level = 1

    -- Guessing state.
    local already_guessed_chars = {}
    local misses = 0
    local accept_input = true   -- Input is not accepted during animation.
    local function reset_guess_state()
        accept_input = true
        already_guessed_chars = {}
        misses = 0
    end

    --
    -- Show next question.
    --
    local function goto_next_question()
        clear_miss_props()

        q = trivia.next_question()
        category_show(q.category)
        page_next(q, function()
            reset_guess_state()
        end)
    end

    MOAIInputMgr.device.keyboard:setCallback(function(key, down)    
        if not down then
            return
        end
        if key == KEY_ESCAPE then
            os.exit()
        end

        -- Ignore keys outside range.
        if key < 48 or key > 122 then
            return
        end

        -- Convert key code to uppercase ASCII char.
        local char = string.char(key):upper()

        -- Special debug key toggles line display.
        if char == "]" then
            return toggle_debug_lines()
        end

        -- Disregard invalid chars. Ignore char if currently not accepting
        -- input.
        if not char:match(trivia.input_chars) or not accept_input then
            return
        end

        if already_guessed_chars[char] == nil then
            -- Show ghost.
            local correct = guess(q, char) > 0
            ghost(char, correct, layer, text_style)

            -- Save character so we know it was guessed before.
            already_guessed_chars[char] = correct

            if correct then
                -- See if the whole answer has been guessed.
                local all_correct = not q.guessable:find("_")

                if all_correct then
                    -- Success!
                    play_sound("sound/correct_answer.wav")
                    accept_input = false

                    -- Update score.
                    score = score + 20 - misses
                    score_update(score)

                    -- If entering new level, remove extra miss slots.
                    local new_level = math.floor(score / 100) + 1
                    local game_ends = new_level > 4

                    if not game_ends and new_level > level then
                        -- Advance level.
                        level = new_level
                        max_misses = max_misses - 2
                        remove_extra_slots()
                    end

                    moai.delayed(0.4, function()
                        page_update_answer(q.guessable)

                        moai.delayed(1.3, function()
                            if not game_ends then
                                goto_next_question()
                            else
                                -- Clean up and go to ending screen.
                                score_hide()
                                clear_miss_props()
                                max_misses = 0
                                remove_extra_slots()
                                category_show(nil)
                                page_hide(function()
                                    ending_screen()
                                end)
                            end
                        end)
                    end)
                else
                    play_sound("sound/correct_char.ogg")
                    moai.delayed(0.3, function()
                        page_update_answer(q.guessable)
                    end)
                end
            else
                -- Add miss.
                add_miss_prop(char, misses)
                misses = misses + 1

                if misses >= max_misses then
                    -- Too bad.. max misses reached.
                    play_sound("sound/reveal.ogg")
                    accept_input = false

                    moai.delayed(0.4, function()
                        page_update_answer(q.answer)

                        -- Go to next question.
                        moai.delayed(1.3, function()
                            goto_next_question()
                        end)
                    end)
                end
            end
        else
            -- User already guessed the char before. Show ghost anyway.
            ghost(char, already_guessed_chars[char], layer, text_style)
        end
    end)
end