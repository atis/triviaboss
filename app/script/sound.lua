
MOAIUntzSystem.initialize()

local music

function music_play(filename, volume, loop, fade_out_time, fade_in_time)
    assert(is_goodstr(filename))
    assert(is_bool(loop))
    assert(is_number(fade_out_time))
    assert(is_number(fade_in_time))

    volume = AppConfig.music_volume * (volume or 1)
    if volume == 0 then
        return
    end

    local function fade_in()
        music = MOAIUntzSound.new()
        music:load(filename)
        music:setLooping(loop)

        if fade_in_time > 0 then
            music:setVolume(0)
            music:seekVolume(volume, fade_in_time)
        else
            music:setVolume(volume)
        end

        music:play()
    end

    if music then
        local prev_music = music
        local drv = prev_music:seekVolume(0, fade_out_time)
        if drv then
            drv:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
                prev_music:stop()
            end)
        end
        music = nil
    end

    fade_in()
end

function music_stop(fade_out_time)
    assert(fade_out_time >= 0)

    if not music then
        return
    end

    local prev_music = music
    local drv = prev_music:seekVolume(0, fade_out_time)
    if drv then
        drv:setListener(MOAITimer.EVENT_TIMER_END_SPAN, function()
            prev_music:stop()
        end)
    end
    music = nil
end

function play_sound(filename, volume, position)
    assert(is_goodstr(filename))
    
    position = position or 0
    volume = AppConfig.sound_volume * (volume or 1)
    if volume == 0 then
        return
    end

    local sound = MOAIUntzSound.new()
    sound:load(filename)
    sound:setLooping(false)
    sound:setVolume(volume)
    sound:setPosition(position)
    sound:play()
end
