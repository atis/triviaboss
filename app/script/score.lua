
local base_tf, scissor_tf, score_tb

local onscreen_pos = {x=710, y=-50}
local offscreen_pos = {x=1030, y=-50}

local score_width, score_height = 350, 200

--
-- Sliding animations.
--
function score_slide_in(callback)
    moai.delayed(0.5, function()
        moai.anim_pos(base_tf, onscreen_pos, 0.4, {
            done = callback
        })
    end)
end
local function DBG_score_slide_in(callback)
    base_tf:setLoc(onscreen_pos.x, onscreen_pos.y)
    if callback then
        callback()
    end
end
function score_slide_out(callback)
    moai.anim_pos(base_tf, offscreen_pos, 0.9, {
        done = callback
    })
end
local function DBG_score_slide_out(callback)
    base_tf:setLoc(offscreen_pos.x, offscreen_pos.y)
    if callback then
        callback()
    end
end
function score_slide_down(callback)
    moai.delayed(0.3, function()
        moai.anim_pos(scissor_tf, {x=0, y=-90}, 0.6, {
            done = callback
        })
    end)
end
local function DBG_score_slide_down(callback)
    scissor_tf:setLoc(0, -90)
    if callback then
        callback()
    end
end
function score_slide_up(callback)
    moai.anim_pos(scissor_tf, {x=0, y=0}, 0.9, {
        done = callback
    })
end
local function DBG_score_slide_up(callback)
    scissor_tf:setLoc(0, 0)
    if callback then
        callback()
    end
end

-- Replace regular animation functions with their debug counterparts (instant
-- transition).
if AppConfig.debug then
    score_slide_in   = DBG_score_slide_in
    score_slide_out  = DBG_score_slide_out
    score_slide_down = DBG_score_slide_down
    score_slide_up   = DBG_score_slide_up
end

--
-- Show score for the first time.
--
function score_show(callback)
    score_slide_in(function()
        score_slide_down(callback)
    end)
end

--
-- Hide score.
--
function score_hide(callback)
    score_slide_up(function()
        score_slide_out(callback)
    end)
end

local function score_string(n)
    assert(n >= 0)

    local level = math.min(math.floor(n / 100) + 1, 4)
    local threshold = math.min(level * 100, 400)
    return "SCORE: "..n.."/"..threshold.."\n"..
           "LEVEL: "..level.."/4"
end

--
-- Set score.
--
function score_update(n)
    score_tb:setString(score_string(n))
end

function score_create(layer, misc_tex, arcade_style)
    assert(is_MOAI_layer(layer))
    assert(is_PackedTexture(misc_tex))
    assert(is_MOAI_textstyle(arcade_style))

    base_tf = moai.new_tf({
        pos = offscreen_pos
    })
    scissor_tf = moai.new_tf({
        parent = base_tf
    })

    --
    -- Scissor effect.
    --
    local scissor = moai.new_scissor({
        parent = scissor_tf,
        pos    = {x=0, y=score_height},
        size   = {width=score_width + 20, height=score_height},
    })

    score_tb = moai.new_textbox({
        layer   = layer,
        parent  = base_tf,
        style   = arcade_style,
        text    = score_string(0),
        size    = {width=score_width, height=score_height},
        pos     = {x=20},
        round   = true,
        h_align = "left",
        scissor = scissor
    })

    local bar = moai.new_quadprop({
        layer  = layer,
        ptex   = misc_tex,
        parent = scissor_tf,
        frame  = "score_bar",
        pos    = {x=-15, y=25}
        --pos    = {x=-15, y=-60}
    })
end
