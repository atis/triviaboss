
require("lib/moai_util")
require("trivia")

local base_tf, bar, scissor_tf
local category_tb, question_tb, answer_tb

local page_width = 650
local page_height = 570

local page_offscreen_pos = {x=-700, y=0}
local page_onscreen_pos = {x=40, y=0}

local scissor_top_pos = {x=0, y=-25}
local scissor_bottom_pos = {x=0, y=-page_height}

--
-- Sliding animations.
--
function page_slide_down(callback)
    moai.anim_pos(scissor_tf, scissor_bottom_pos, 0.9, {
        from   = scissor_top_pos,
        done   = callback
    })
end
local function DBG_page_slide_down(callback)
    scissor_tf:setLoc(scissor_bottom_pos.x, scissor_bottom_pos.y)
    if callback then
        callback()
    end
end
function page_slide_up(callback)
    moai.anim_pos(scissor_tf, scissor_top_pos, 0.9, {
        from   = scissor_bottom_pos,
        done   = callback
    })
end
local function DBG_page_slide_up(callback)
    scissor_tf:setLoc(scissor_top_pos.x, scissor_top_pos.y)
    if callback then
        callback()
    end
end
function page_slide_in(callback)
    moai.anim_pos(base_tf, page_onscreen_pos, 1, {
        ease   = MOAIEaseType.EASE_IN,
        done   = callback
    })
end
local function DBG_page_slide_in(callback)
    base_tf:setLoc(page_onscreen_pos.x, page_onscreen_pos.y)
    if callback then
        callback()
    end
end
function page_slide_out(callback)
    moai.anim_pos(base_tf, page_offscreen_pos, 1, {
        ease   = MOAIEaseType.EASE_OUT,
        done   = callback
    }) 
end
local function DBG_page_slide_out(callback)
    base_tf:setLoc(page_onscreen_pos.x, page_onscreen_pos.y)
    if callback then
        callback()
    end
end

-- Replace regular animation functions with their debug counterparts (instant
-- transition).
if AppConfig.debug then
    page_slide_down = DBG_page_slide_down
    page_slide_up   = DBG_page_slide_up
    page_slide_in   = DBG_page_slide_in
    page_slide_out  = DBG_page_slide_out
end

local function set_next_question(q)
    assert(is_table(q))

    category_tb:setString("Category: "..q.category)
    question_tb:setString(q.question)
    answer_tb:setString(q.guessable)
end

--
-- Flip to next question.
--
function page_next(q, callback)
    assert(is_table(q))

    page_slide_up(function()
        set_next_question(q)
        page_slide_down(callback)
    end)
end

--
-- Show first question.
--
function page_show_first(q, callback)
    assert(is_table(q))

    page_slide_in(function()
        set_next_question(q)
        page_slide_down(callback)
    end)
end

--
-- Hide page.
--
function page_hide(callback)
    page_slide_up(function()
        page_slide_out(callback)
    end)
end

--
-- Set answer text to something new.
--
function page_update_answer(guessable)
    assert(is_goodstr(guessable))
    answer_tb:setString(guessable)
end

function page_create(layer, misc_tex, arcade_style)
    assert(is_MOAI_layer(layer))
    assert(is_PackedTexture(misc_tex))
    assert(is_MOAI_textstyle(arcade_style))

    -- Page base transform.
    base_tf = moai.new_tf({
        pos = page_offscreen_pos
    })

    -- Scissor effect.
    scissor_tf = moai.new_tf {
        parent = base_tf,
        pos    = scissor_top_pos
    }
    local scissor = moai.new_scissor({
        parent = scissor_tf,
        pos    = {x=0, y=-25+page_height},
        size   = {width=page_width + 30, height=page_height},
    })

    category_tb = moai.new_textbox({
        layer   = layer,
        parent  = base_tf,
        style   = arcade_style,
        size    = {width=page_width, height=100},
        pos     = {y = -50},
        round   = true,
        h_align = "left",
        scissor = scissor
    })

    question_tb = moai.new_textbox({
        layer   = layer,
        parent  = base_tf,
        style   = arcade_style,
        size    = {width=page_width, height=400},
        pos     = {y = -120},
        round   = true,
        h_align = "left",
        scissor = scissor
    })

    answer_tb = moai.new_textbox({
        layer   = layer,
        parent  = base_tf,
        style   = arcade_style,
        size    = {width=page_width, height=200},
        pos     = {y = -360},
        round   = true,
        h_align = "center",
        v_align = "bottom",
        scissor = scissor
    })

    bar = moai.new_quadprop({
        layer  = layer,
        ptex   = misc_tex,
        parent = scissor_tf,
        frame  = "page_bar",
        pos    = {x=-26, y=0}
    })
end